package de.mo.TodoList.classic;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.UI;

import de.mo.TodoList.MyUI;
import de.mo.TodoList.helper.ObjectToString;
import de.mo.TodoList.model.Todo;

public class TodoPresenter {
	
	private String input;
	private List<Todo> todos;
	private TodoView view;
	
	public TodoPresenter(TodoView view) {
		this.view = view;
		this.todos = new ArrayList<Todo>();
		this.loadTodos();
		
		view.addInputChangeListener(changeEvent -> {
			this.input = (String) changeEvent.getProperty().getValue();
		});
		
		view.addSelectionListener(selection -> {
			selection.getSelected()
				.stream().forEach(todo -> ((Todo) todo).toggleCompleted());
		});
		
		view.addSumbitListener(clickEvent -> {
			if(this.input != null) {
				Todo todo = new Todo(this.input);
				this.todos.add(todo);
				view.addTodo(todo);
				saveTodos(todos);
			}
		});
		
		view.addDeleteListener(todo -> {
			this.todos.remove(todo);
			view.removeTodo(todo);
			saveTodos(todos);
		});
	
	}
	
	/**
	 * save Todos to local storage
	 * @param todos
	 */
	public void saveTodos(List<Todo> todos) {
		try {
			String todosString = ObjectToString.toString((Serializable) todos);
			((MyUI) UI.getCurrent()).saveToLocalStorage("todos", todosString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * load todos from localStorage
	 */
	@SuppressWarnings("unchecked")
	public void loadTodos() {
		((MyUI) UI.getCurrent()).loadFromLocalStorage("todos", todosString -> {
			if(todosString != null) {
				try {
					List<Todo> todos = (List<Todo>) ObjectToString.fromString(todosString);
					this.todos = todos;
					for(Todo todo : todos) {
						view.addTodo(todo);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		});

	}

}
