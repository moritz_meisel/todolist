package de.mo.TodoList;

import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;

import com.github.wolfie.clientstorage.ClientStorage;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

import de.mo.TodoList.classic.TodoPresenter;
import de.mo.TodoList.classic.TodoView;
import de.mo.TodoList.model.Todo;
import de.mo.TodoList.model.TodoModel;
import de.mo.TodoList.reactive.ReactiveTodoPresenter;
import de.mo.TodoList.reactive.ReactiveTodoView;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
@Widgetset("de.mo.TodoList.MyAppWidgetset")
public class MyUI extends UI {
	
	private ClientStorage clientStorage;

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		Responsive.makeResponsive(this);
		clientStorage = new ClientStorage(supported -> {
	    	   System.out.println("supported");
		});
		addExtension(clientStorage);
			
        getPage().setTitle("My");
//        showTodoView();
        showReactiveTodoView();
    }
	
	/**
	 * save to local storage
	 * @param key
	 * @param value
	 */
	public void saveToLocalStorage(String key, String value) {
		clientStorage.setLocalItem(key, value);
	}
	
	/**
	 * save to local storage
	 * @param key
	 * @param value
	 */
	public void loadFromLocalStorage(String key, ClientStorage.Closure value) {
		clientStorage.getLocalItem(key, value);
	}

	/**
	 * show the todo view
	 */
    private void showTodoView() {
    	TodoView todoView = new TodoView(MyUI.this);
    	TodoPresenter presenter = new TodoPresenter(todoView);
    	setContent(todoView);
	}
    
    /**
	 * show reactive the todo view
	 */
    private void showReactiveTodoView() {
    	ReactiveTodoView todoView = new ReactiveTodoView(MyUI.this);
    	TodoModel model = new TodoModel(new ArrayList<Todo>(), "");
    	ReactiveTodoPresenter presenter = new ReactiveTodoPresenter(model, todoView);
    	todoView.setTodoStream(presenter.getState());
    	setContent(todoView);
	}

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
