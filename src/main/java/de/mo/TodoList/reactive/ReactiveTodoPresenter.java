package de.mo.TodoList.reactive;



import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import com.vaadin.ui.UI;

import de.mo.TodoList.MyUI;
import de.mo.TodoList.helper.ObjectToString;
import de.mo.TodoList.model.Todo;
import de.mo.TodoList.model.TodoModel;
import de.mo.TodoList.reactive.architecture.Action;
import de.mo.TodoList.reactive.architecture.ActionDTO;
import de.mo.TodoList.reactive.architecture.Presenter;
import rx.Observable;

@SuppressWarnings("rawtypes")
public class ReactiveTodoPresenter extends Presenter<TodoModel> {
	
	public static enum Actions implements Action {
		SET_TODOS,
	    SUBMIT_TODO,
	    INPUT_CHANGE,
	    TOGGLE_TODO,
	    DELETE_TODO
	}
	
	public ReactiveTodoPresenter(TodoModel initalValue, ReactiveTodoView view) {
		super(initalValue);
		
		loadTodos();
		
		// map submit click to submit action
		Observable<ActionDTO> submitAction = view.getSubmitObervable()
				.map(clickEvent -> createSubmitAction());
		// map changed input to input action
		Observable<ActionDTO> todoChanged = view.getMessageObservable()
				.map(valueChangeEvent ->
					createInputAction((String) valueChangeEvent.getProperty().getValue()));
		// map toggle event to toggle action
		Observable<ActionDTO> toggleTodos = view.getToggledTodo()
				.map(todo -> createToggleAction(todo));
		// map toggle event to toggle action
		Observable<ActionDTO> deleteTodo = view.getDeleteTodo()
				.map(todo -> createDeleteAction(todo));
		// merge all actions and send to action bus
		Observable<ActionDTO> actions = 
				Observable.<ActionDTO>merge(submitAction, todoChanged, toggleTodos, deleteTodo);
		actions.subscribe(action -> this.doAction(action));		
	}

	@Override
	public TodoModel update(TodoModel model, ActionDTO actionDTO) {
		Action action = actionDTO.action;
        System.out.println(actionDTO.action.toString());
        switch((Actions) action) {
        	case SET_TODOS:
            	model.todos = (List<Todo>) actionDTO.payload;
            	break;
            case SUBMIT_TODO:
            	Todo todo = new Todo(model.input);
            	model.todos.add(todo);
                saveTodos(model.todos);
                break;
            case INPUT_CHANGE:
            	model.input = (String) actionDTO.payload;
            	break;
            case TOGGLE_TODO:
            	todo = (Todo) actionDTO.payload;
            	todo.toggleCompleted();
                saveTodos(model.todos);
                break;
            case DELETE_TODO:
            	todo = (Todo) actionDTO.payload;
            	model.todos.remove(todo);
                saveTodos(model.todos);
                break;
        }
		return model;
	}
	
	// action creators
	private ActionDTO<String> createInputAction(String value) {
		return new ActionDTO<String>(Actions.INPUT_CHANGE,	value, String.class);
	}
	
	private ActionDTO<Void> createSubmitAction() {
		return new ActionDTO<Void>(Actions.SUBMIT_TODO);
	}
	
	private ActionDTO<Todo> createToggleAction(Todo todo) {
		return new ActionDTO<Todo>(Actions.TOGGLE_TODO, todo, Todo.class);
	}
	
	private ActionDTO<Todo> createDeleteAction(Todo todo) {
		return new ActionDTO<Todo>(Actions.DELETE_TODO, todo, Todo.class);
	}
	
	/**
	 * save Todos to local storage
	 * @param todos
	 */
	public void saveTodos(List<Todo> todos) {
		try {
			String todosString = ObjectToString.toString((Serializable) todos);
			((MyUI) UI.getCurrent()).saveToLocalStorage("todos", todosString);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * load todos from localStorage
	 */
	@SuppressWarnings("unchecked")
	public void loadTodos() {
		((MyUI) UI.getCurrent()).loadFromLocalStorage("todos", todosString -> {
			if(todosString != null) {
				try {
					List<Todo> todos = (List<Todo>) ObjectToString.fromString(todosString);
					 this.doAction(new ActionDTO<List>(Actions.SET_TODOS, todos, List.class));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
		});

	}

}
