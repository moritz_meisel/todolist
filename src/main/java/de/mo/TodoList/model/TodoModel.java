package de.mo.TodoList.model;

import java.util.List;

public class TodoModel {
	public List<Todo> todos;
	public String input;
	public Integer filter = 0;
	
	public TodoModel() {
		
	}
	
	public TodoModel(List<Todo> messages, String input) {
		this.todos = messages;
		this.input = input;
	}

	public List<Todo> getTodos() {
		return todos;
	}

	public void setTodos(List<Todo> todos) {
		this.todos = todos;
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public Integer getFilter() {
		return filter;
	}

	public void setFilter(Integer filter) {
		this.filter = filter;
	}

}
